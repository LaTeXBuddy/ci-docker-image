# LaTeXBuddy CI Docker Image

This is a Docker Image that we use to test LaTeXBuddy. It _does not_ come with
LaTeXBuddy preinstalled, but with some of its dependencies:

- TeX Live,
- GNU Aspell,
- ChkTeX,
- GNU Diction,

along with Git and Python.

## Usage

The image is built and pushed by GitLab on every commit or
[manual pipeline run](https://gitlab.com/LaTeXBuddy/ci-docker-image/-/pipelines/new).
Each branch corresponds to the target Python version.

You can pull [the image](https://gitlab.com/LaTeXBuddy/ci-docker-image/container_registry/4058793)
from the GitLab Container Registry:

```sh
docker pull registry.gitlab.com/latexbuddy/ci-docker-image:latest
```

## Tags

- `latest` - ships with the latest Python version
- `3.11` - ships with Python 3.11
- `3.10` - ships with Python 3.10
- `3.9` - ships with Python 3.9
- `3.8` - ships with Python 3.8
- `3.7` - ships with Python 3.7

## Licence
© 2023 LaTeXBuddy\
Licenced under the [Apache License 2.0][apache-2.0].

---

This project is hosted on GitLab:
<https://gitlab.com/LaTeXBuddy/ci-docker-image.git>

[apache-2.0]: https://spdx.org/licenses/Apache-2.0.html
