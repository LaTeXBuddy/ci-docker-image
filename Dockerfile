ARG PYTHON_VERSION=3

#
# ChkTeX
#
FROM alpine:latest AS build-chktex

# install GCC and other libs for building
RUN : \
    && apk update \
    && apk add \
        build-base \
        git \
        perl \
    && :

# download chktex
RUN : \
    && wget 'http://download.savannah.gnu.org/releases/chktex/chktex-1.7.8.tar.gz' \
    && tar -xzf chktex-1.7.8.tar.gz \
    && :

# build chktex
WORKDIR /chktex-1.7.8
RUN : \
    && ./configure --prefix /usr/local \
    && make \
    && chmod 644 chktexrc \
    && :

#
# Diction
#
FROM alpine:latest AS build-diction

# install GCC and other libs for building
RUN : \
    && apk update \
    && apk add \
        build-base \
        git \
    && :

# download diction
RUN : \
    && wget 'http://www.moria.de/~michael/comp/diction/diction-1.14.tar.gz' \
    && tar -xzf diction-1.14.tar.gz \
    && :

# build diction
WORKDIR /diction-1.14
RUN : \
    && ./configure --prefix /usr/local \
    && make \
    && :

#
# Actual image
#
FROM python:${PYTHON_VERSION}-alpine

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN : \
    && apk update \
    && apk add \
        git \
        aspell \
        aspell-en \
        aspell-de \
        libstdc++ \
        texlive \
    && :

# install chktex
COPY --from=build-chktex /chktex-1.7.8/chktex /usr/local/bin/chktex
COPY --from=build-chktex /chktex-1.7.8/chktexrc /usr/local/etc/chktexrc

# install diction
COPY --from=build-diction /diction-1.14/diction /usr/local/bin/diction

CMD ["python3"]
